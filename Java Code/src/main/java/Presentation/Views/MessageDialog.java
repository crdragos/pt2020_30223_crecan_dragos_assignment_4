package Presentation.Views;

import javax.swing.*;

public class MessageDialog extends JFrame {

    protected JPanel contentPanel;
    protected JLabel messageLabel;
    protected JButton okButton;

    public MessageDialog(String message) {
        this.setSize(Constants.Dialogs.DeleteDialogWidth, Constants.Dialogs.DeleteDialogHeight);
        this.setLayout(null);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setUndecorated(true);
        this.getRootPane().setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Constants.Colors.ToolbarColor));
        this.setResizable(false);
        this.setVisible(true);

        contentPanel = new JPanel();
        contentPanel.setBounds(0, 0, Constants.Dialogs.DeleteDialogWidth, Constants.Dialogs.DeleteDialogHeight);
        contentPanel.setLayout(null);
        contentPanel.setBackground(Constants.Colors.BackgroundColor);
        this.add(contentPanel);

        messageLabel = new JLabel(message);
        messageLabel.setBounds(0, 25, 550, 50);
        messageLabel.setFont(Constants.Fonts.SmallFont);
        messageLabel.setHorizontalAlignment(JLabel.CENTER);
        messageLabel.setVerticalAlignment(JLabel.CENTER);
        contentPanel.add(messageLabel);

        okButton = new JButton("Ok");
        okButton.setBounds(225, 125, 100, 30);
        okButton.setBackground(Constants.Colors.ButtonsColor);
        okButton.setFont(Constants.Fonts.SmallFont);
        okButton.addActionListener(e -> {
            this.dispose();
        });
        this.contentPanel.add(okButton);
    }
}
