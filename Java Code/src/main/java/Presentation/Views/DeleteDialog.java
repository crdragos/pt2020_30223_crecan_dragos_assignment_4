package Presentation.Views;

import BLL.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class DeleteDialog extends JFrame {

    private JPanel contentPanel;
    private JButton confirmButton;
    private JButton cancelButton;
    private JLabel messageLabel;

    public DeleteDialog() {

        this.setSize(300, Constants.Dialogs.DeleteDialogHeight);
        this.setLayout(null);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setUndecorated(true);
        this.getRootPane().setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Constants.Colors.ToolbarColor));
        this.setResizable(false);
        this.setVisible(true);

        contentPanel = new JPanel();
        contentPanel.setBounds(0, 0, Constants.Dialogs.DeleteDialogWidth, Constants.Dialogs.DeleteDialogHeight);
        contentPanel.setLayout(null);
        contentPanel.setBackground(Constants.Colors.BackgroundColor);
        this.add(contentPanel);

        messageLabel = new JLabel("Are you sure?");
        messageLabel.setBounds(50, 25, 200, 50);
        messageLabel.setFont(Constants.Fonts.LabelsFont);
        messageLabel.setHorizontalAlignment(JLabel.CENTER);
        messageLabel.setVerticalAlignment(JLabel.CENTER);
        contentPanel.add(messageLabel);

        confirmButton = new JButton("Confirm");
        confirmButton.setBounds(20, 125, 100, 30);
        confirmButton.setFont(Constants.Fonts.SmallFont);
        confirmButton.setBackground(Constants.Colors.ButtonsColor);
        confirmButton.addActionListener(e -> {
            RestaurantFrame.deleteMenuTable();

            String productName = String.valueOf(RestaurantFrame.getMenuTable().getModel().getValueAt(RestaurantFrame.getMenuTable().getSelectedRow(), 0));
            MenuItem toDeleteMenuItem = RestaurantFrame.getRestaurant().search(productName);
            RestaurantFrame.getRestaurant().deleteMenuItem(toDeleteMenuItem);

            RestaurantFrame.displayMenuTable();
            this.dispose();
        });
        this.contentPanel.add(confirmButton);

        cancelButton = new JButton("Cancel");
        cancelButton.setBounds(170, 125, 100, 30);
        cancelButton.setFont(Constants.Fonts.SmallFont);
        cancelButton.setBackground(Constants.Colors.BackgroundColor);
        cancelButton.setForeground(Color.RED);
        cancelButton.setBorderPainted(false);
        cancelButton.addActionListener(e -> {
            this.dispose();
        });
        this.contentPanel.add(cancelButton);
    }

    public void setConfirmButtonActionListener(ActionListener actionListener) {
        confirmButton.addActionListener(actionListener);
    }
}
