package Presentation.Views;

import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public final class Constants {

    public static final class Frames {

        public static final int WIDTH = 900;
        public static final int HEIGHT = 650;
    }

    public static final class Dialogs {
        public static final int WIDTH = 650;
        public static final int HEIGHT = 450;
        public static final int DeleteDialogWidth = 550;
        public static final int DeleteDialogHeight = 200;
        public static final int NewOrderDialogWidth = 800;
        public static final int NewOrderDialogHeight = 500;
    }

    public static final class Colors {

        public static final Color BackgroundColor = new Color(230, 204, 181);
        public static final Color ToolbarColor = new Color(186, 168, 150);
        public static final Color ButtonsColor = new Color(252, 200, 117);
        public static final Color TextColor = new Color(222, 122, 34);
        public static final Color BLACK = new Color(0, 0, 0);
        public static final Color TabelBackground = new Color(252, 200, 117);
    }

    public static final class Fonts {

        public static final Font LabelsFont = new Font("Times New Roman", Font.BOLD | Font.ITALIC, 30);
        public static final Font ButtonsFont = new Font("Times New Roman", Font.BOLD | Font.ITALIC, 18);
        public static final Font SmallFont = new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16);
        public static final Font TextFiledsFont = new Font("Times New Roman", Font.ITALIC, 18);
        public static final Font TableFont = new Font("Times New Roman", Font.ITALIC | Font.BOLD, 18);
    }

    public static final class Tables {
        public static final Object[] menuTableHeader = new Object[] {"Name", "Product Type", "Price"};
        public static final Object[] orderTableHeader = new Object[] {"Order ID", "Table", "Date", "Price"};
        public static final Object[] chefTableHeader = new Object[] {"Product Name", "Product Type", "Quantity"};

        public static final int scrollPaneWidth = 650;
        public static final int scrollPaneMaximHeight = 400;
        public static final int rowHeight = 25;

        public static final Dimension tableHeaderDimension = new Dimension(scrollPaneWidth, 35);

    }
}
