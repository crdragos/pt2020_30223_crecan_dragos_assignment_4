package Presentation.Views;

import BLL.MenuItem;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;
import java.util.HashSet;

public class ComponentsDialog extends JFrame {

    private JLabel titleLabel;

    private JButton addButton;
    private JButton doneButton;

    private static DefaultTableModel defaultTableModel;
    private static JTable table;
    private static JScrollPane scrollPane;

    private JPanel contentPanel;

    private static HashSet<MenuItem> productComponents;

    public ComponentsDialog(DefaultTableModel defaultTableModel, JTable table, JScrollPane scrollPane) {
        this.setSize(Constants.Frames.WIDTH, 650);
        this.setLayout(null);
        this.setUndecorated(true);
        this.setBackground(Constants.Colors.BackgroundColor);
        this.getRootPane().setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Constants.Colors.ToolbarColor));
        this.setLocationRelativeTo(null);

        productComponents = new HashSet<>();

        this.defaultTableModel = defaultTableModel;
        this.table = table;
        this.scrollPane = scrollPane;

        contentPanel = new JPanel();
        contentPanel.setLayout(null);
        contentPanel.setBounds(0, 0, Constants.Frames.WIDTH, Constants.Frames.HEIGHT);
        contentPanel.setBackground(Constants.Colors.BackgroundColor);
        this.add(contentPanel);

        titleLabel = new JLabel("Add components");
        titleLabel.setBounds(300, 55, 300, 50);
        titleLabel.setFont(Constants.Fonts.LabelsFont);
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setVerticalAlignment(JLabel.CENTER);
        contentPanel.add(titleLabel);

        this.scrollPane.setVisible(true);
        contentPanel.add(scrollPane);

        addButton = new JButton("Add");
        addButton.setBounds(300, 550, 100, 50);
        addButton.setBackground(Constants.Colors.ButtonsColor);
        addButton.setFont(Constants.Fonts.ButtonsFont);
        contentPanel.add(addButton);

        doneButton = new JButton("Done");
        doneButton.setBounds(500, 550, 100, 50);
        doneButton.setBackground(Constants.Colors.ButtonsColor);
        doneButton.setFont(Constants.Fonts.ButtonsFont);
        doneButton.addActionListener(e -> {
            this.dispose();
            RestaurantFrame.deleteMenuTable();
            RestaurantFrame.displayMenuTable();
        });
        contentPanel.add(doneButton);

        this.setVisible(true);
    }

    public static DefaultTableModel getDefaultTableModel() {
        return defaultTableModel;
    }

    public static JTable getTable() {
        return table;
    }

    public static JScrollPane getScrollPane() {
        return scrollPane;
    }

    public static HashSet<MenuItem> getProductComponents() {
        return productComponents;
    }

    public void setAddButtonActionListener(ActionListener actionListener) {
        addButton.addActionListener(actionListener);
    }
}
