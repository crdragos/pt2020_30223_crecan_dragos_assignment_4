package Presentation.Views;

import BLL.BaseProduct;
import BLL.CompositeProduct;
import BLL.MenuItem;
import Presentation.Controllers.RestaurantController;
import Run.MainClass;

import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.regex.Pattern;

public class AddComponentDialog extends JFrame {

    private JPanel contentPanel;
    private JPanel buttonsPannel;
    private JPanel tablePanel;

    private JButton addComponentsButton;
    private JButton doneButton;
    private JButton cancelButton;

    private JLabel titleLabel;

    private JLabel nameLabel;
    private JTextField nameTextField;
    private JLabel priceLabel;
    private static JTextField priceTextField;

    private static DefaultTableModel defaultTableModel;
    private static JTable table;
    private static JScrollPane scrollPane;

    private HashSet<MenuItem> menuItems;
    private static HashSet<MenuItem> newComponents;

    private static String newProductName;
    private static String oldProductName;
    private Float productPrice;

    public AddComponentDialog() {
        this.setSize(Constants.Dialogs.NewOrderDialogWidth, Constants.Dialogs.NewOrderDialogHeight);
        this.setLayout(null);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setUndecorated(true);
        this.getRootPane().setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Constants.Colors.ToolbarColor));
        this.setResizable(false);
        this.setVisible(true);

        this.menuItems = RestaurantFrame.getRestaurant().getMenuItems();
        this.newProductName = EditDialog.getNameTextField().getText();
        this.oldProductName = String.valueOf(RestaurantFrame.getMenuTable().getModel().getValueAt(RestaurantFrame.getMenuTable().getSelectedRow(), 0));
        this.productPrice = RestaurantFrame.getRestaurant().search(oldProductName).getPrice();
        this.newComponents = RestaurantFrame.getRestaurant().search(oldProductName).getComponents();

        contentPanel = new JPanel();
        contentPanel.setBounds(0, 0, Constants.Dialogs.NewOrderDialogWidth, Constants.Dialogs.NewOrderDialogHeight);
        contentPanel.setLayout(null);
        contentPanel.setBackground(Constants.Colors.BackgroundColor);
        this.add(contentPanel);

        titleLabel = new JLabel("Add Components to Composite Product");
        titleLabel.setBounds(0, 5, Constants.Dialogs.NewOrderDialogWidth, 40);
        titleLabel.setFont(Constants.Fonts.LabelsFont);
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setVerticalAlignment(JLabel.CENTER);
        titleLabel.setVisible(true);
        contentPanel.add(titleLabel);

        buttonsPannel = new JPanel();
        buttonsPannel.setBounds(0, 50, 300, 450);
        buttonsPannel.setLayout(null);
        buttonsPannel.setBackground(Constants.Colors.BackgroundColor);
        contentPanel.add(buttonsPannel);

        nameLabel = new JLabel("Name");
        nameLabel.setBounds(0, 55, 100, 30);
        nameLabel.setFont(Constants.Fonts.LabelsFont);
        nameLabel.setHorizontalAlignment(JLabel.CENTER);
        nameLabel.setVerticalAlignment(JLabel.CENTER);
        nameLabel.setVisible(true);
        buttonsPannel.add(nameLabel);

        nameTextField = new JTextField();
        nameTextField.setBounds(120, 55, 170, 30);
        nameTextField.setFont(Constants.Fonts.TextFiledsFont);
        nameTextField.setHorizontalAlignment(JTextField.CENTER);
        nameTextField.setText(oldProductName);
        nameTextField.setEditable(true);
        nameTextField.setVisible(true);
        buttonsPannel.add(nameTextField);

        priceLabel = new JLabel("Price");
        priceLabel.setBounds(0, 120, 100, 30);
        priceLabel.setFont(Constants.Fonts.LabelsFont);
        priceLabel.setHorizontalAlignment(JLabel.CENTER);
        priceLabel.setVerticalAlignment(JLabel.CENTER);
        priceLabel.setVisible(true);
        buttonsPannel.add(priceLabel);

        priceTextField = new JTextField();
        priceTextField.setBounds(120, 120, 170, 30);
        priceTextField.setFont(Constants.Fonts.TextFiledsFont);
        priceTextField.setHorizontalAlignment(JTextField.CENTER);
        priceTextField.setText(String.valueOf(productPrice));
        priceTextField.setEditable(false);
        priceTextField.setVisible(true);
        buttonsPannel.add(priceTextField);

        addComponentsButton = new JButton("Add Components");
        addComponentsButton.setBounds(10, 180, 280, 40);
        addComponentsButton.setBackground(Constants.Colors.ButtonsColor);
        addComponentsButton.setFont(Constants.Fonts.ButtonsFont);
        addComponentsButton.setHorizontalAlignment(JButton.CENTER);
        addComponentsButton.setVerticalAlignment(JButton.CENTER);
        addComponentsButton.setVisible(true);
        buttonsPannel.add(addComponentsButton);

        doneButton = new JButton("Done");
        doneButton.setBounds(10, 300, 100, 40);
        doneButton.setBackground(Constants.Colors.ButtonsColor);
        doneButton.setFont(Constants.Fonts.ButtonsFont);
        doneButton.setHorizontalAlignment(JButton.CENTER);
        doneButton.setVerticalAlignment(JButton.CENTER);
        doneButton.setVisible(true);
        doneButton.addActionListener(e -> {
            EditDialog.getNameTextField().setText(newProductName);
            MenuItem oldItem = RestaurantFrame.getRestaurant().search(oldProductName);

            newProductName = nameTextField.getText();
            if (Pattern.matches("[a-z -?A-z]+", newProductName)) {
                MenuItem newItem = new CompositeProduct(newProductName, newComponents);
                RestaurantFrame.getRestaurant().editMenuItem(oldItem, newItem);

                RestaurantFrame.getRestaurant().getRestaurantSerilialization().write(RestaurantFrame.getRestaurant().getMenuItems(), MainClass.fileName);
                RestaurantFrame.getRestaurant().setMenuItems(RestaurantFrame.getRestaurant().getRestaurantSerilialization().read(MainClass.fileName));

                EditDialog.getCancelButton().setEnabled(false);
                EditDialog.getRemoveComponentsButton().setEnabled(false);
                RestaurantFrame.deleteMenuTable();
                RestaurantFrame.displayMenuTable();
                this.dispose();
            } else {
                new MessageDialog("Name must contain only letters!");
            }

        });
        buttonsPannel.add(doneButton);

        cancelButton = new JButton("Cancel");
        cancelButton.setBounds(190, 300, 100, 40);
        cancelButton.setBackground(Constants.Colors.BackgroundColor);
        cancelButton.setFont(Constants.Fonts.ButtonsFont);
        cancelButton.setHorizontalAlignment(JButton.CENTER);
        cancelButton.setVerticalAlignment(JButton.CENTER);
        cancelButton.setForeground(Color.RED);
        cancelButton.setBorderPainted(false);
        cancelButton.setVisible(true);
        cancelButton.addActionListener(e -> {
            this.dispose();
        });
        buttonsPannel.add(cancelButton);

        tablePanel = new JPanel();
        tablePanel.setBounds(300, 50, 500, 450);
        tablePanel.setLayout(null);
        tablePanel.setBackground(Constants.Colors.BackgroundColor);
        contentPanel.add(tablePanel);

        displayMenuTable();
    }

    public void displayMenuTable() {
        defaultTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        defaultTableModel.setColumnIdentifiers(Constants.Tables.menuTableHeader);

        table = new JTable(defaultTableModel);
        table.setBackground(Constants.Colors.TabelBackground);
        table.getTableHeader().setFont(Constants.Fonts.TableFont);
        table.getTableHeader().setBackground(Constants.Colors.TabelBackground);
        table.getTableHeader().setPreferredSize(Constants.Tables.tableHeaderDimension);
        table.setRowHeight(Constants.Tables.rowHeight);

        scrollPane = new JScrollPane(table);
        scrollPane.setBackground(Constants.Colors.ToolbarColor);
        scrollPane.getVerticalScrollBar().setBackground(Constants.Colors.ToolbarColor);
        scrollPane.getVerticalScrollBar().setUI(new BasicScrollBarUI() {
            @Override
            protected void configureScrollBarColors() {
                this.thumbColor = Constants.Colors.ButtonsColor;
            }

            @Override
            protected JButton createDecreaseButton(int orientation) {
                JButton button = super.createDecreaseButton(orientation);
                button.setBackground(Constants.Colors.ButtonsColor);
                return button;
            }

            @Override
            protected JButton createIncreaseButton(int orientation) {
                JButton button = super.createIncreaseButton(orientation);
                button.setBackground(Constants.Colors.ButtonsColor);
                return button;
            }
        });

        if (RestaurantFrame.getRestaurant().getMenuItems().size() * Constants.Tables.rowHeight + 38 <= 350 ) {
            scrollPane.setBounds(10, 55, 470, RestaurantFrame.getRestaurant().getMenuItems().size() * Constants.Tables.rowHeight + 38);
        } else {
            scrollPane.setBounds(10, 55, 470, 350);
        }
        table.setBackground(Constants.Colors.BackgroundColor);
        scrollPane.setVisible(true);
        tablePanel.add(scrollPane);

        Object[] row = new Object[3];
        for (MenuItem menuItem : menuItems) {
            row[0] = menuItem.getName();
            if (menuItem instanceof BaseProduct) {
                row[1] = "Base Product";
            } else {
                row[1] = "Composite Product";
            }
            row[2] = menuItem.getPrice();
            defaultTableModel.addRow(row);
        }
    }

    public static JTable getTable() {
        return table;
    }

    public static HashSet<MenuItem> getNewComponents() {
        return newComponents;
    }

    public static String getOldProductName() {
        return oldProductName;
    }

    public static JTextField getPriceTextField() {
        return priceTextField;
    }

    public void setAddComponentsButton(ActionListener actionListener) {
        addComponentsButton.addActionListener(actionListener);
    }
}
