package Presentation.Views;

import BLL.BaseProduct;
import BLL.MenuItem;
import BLL.Order;
import BLL.Restaurant;
import DAL.FileWritter;

import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class RestaurantFrame extends JFrame implements Observer {

    private static Restaurant restaurant;
    private static ArrayList<MenuItem> toChef;

    protected JPanel toolbarPanel;
    protected static JPanel contentPanel;
    protected static JPanel waiterActionsPanel;
    protected static JPanel adminActionsPanel;

    protected static JButton adminButton;
    protected static JButton waiterButton;
    protected static JButton chefButton;
    protected static JButton homePageButton;

    protected static JButton addNewItemButton;
    protected static JButton deleteItemButton;
    protected static JButton editItemButton;

    protected static JButton createOrderButton;
    protected static JButton computePriceButton;
    protected static JButton generateBillButton;

    protected static JLabel menuLabel;
    protected static JLabel orderLabel;
    protected static JLabel toCookLabel;

    protected static JLabel welcomeLabel;
    protected static JLabel roleLabel;

    protected static JLabel noMenuItemsLabel;
    protected static JLabel noOrdersLabel;

    protected static JScrollPane menuScrollPane;
    protected static JScrollPane orderScrollPane;
    protected static JScrollPane chefScrollPane;

    protected static DefaultTableModel defaultMenuTableModel;
    protected static DefaultTableModel defaultOrderTableModel;
    protected static DefaultTableModel defaultChefTableModel;

    protected static JTable menuTable;
    protected static JTable orderTable;
    protected static JTable chefTable;

    public RestaurantFrame(String title) {

        toChef = new ArrayList<>();

        this.setTitle(title);
        this.setSize(Constants.Frames.WIDTH, Constants.Frames.HEIGHT);
        this.setLayout(null);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);

        toolbarPanel = new JPanel();
        toolbarPanel.setBackground(Constants.Colors.ToolbarColor);
        toolbarPanel.setBounds(0, 0, Constants.Frames.WIDTH, 50);
        toolbarPanel.setVisible(true);
        toolbarPanel.setLayout(null);
        this.add(toolbarPanel);

        adminButton = new JButton("Administrator");
        adminButton.setBounds(0, 0, 150, 50);
        adminButton.setFont(Constants.Fonts.ButtonsFont);
        adminButton.setBackground(Constants.Colors.ToolbarColor);
        toolbarPanel.add(adminButton);

        waiterButton = new JButton("Waiter");
        waiterButton.setBounds(150, 0, 150, 50);
        waiterButton.setBackground(Constants.Colors.ToolbarColor);
        waiterButton.setFont(Constants.Fonts.ButtonsFont);
        toolbarPanel.add(waiterButton);

        chefButton = new JButton("Chef");
        chefButton.setBounds(300, 0, 150, 50);
        chefButton.setBackground(Constants.Colors.ToolbarColor);
        chefButton.setFont(Constants.Fonts.ButtonsFont);
        toolbarPanel.add(chefButton);

        homePageButton = new JButton("Home Page");
        homePageButton.setBounds(750, 0, 150, 50);
        homePageButton.setBackground(Constants.Colors.ButtonsColor);
        homePageButton.setFont(Constants.Fonts.ButtonsFont);
        toolbarPanel.add(homePageButton);

        contentPanel = new JPanel();
        contentPanel.setBackground(Constants.Colors.BackgroundColor);
        contentPanel.setBounds(0, 50, Constants.Frames.WIDTH, Constants.Frames.HEIGHT - 50);
        contentPanel.setLayout(null);
        this.add(contentPanel);

        adminActionsPanel = new JPanel();
        adminActionsPanel.setBackground(Constants.Colors.BackgroundColor);
        adminActionsPanel.setBounds(0, 0, Constants.Frames.WIDTH, 50);
        adminActionsPanel.setLayout(null);
        adminActionsPanel.setVisible(false);
        contentPanel.add(adminActionsPanel);

        addNewItemButton = new JButton("Add New Item");
        addNewItemButton.setBounds(0, 0, 300, 50);
        addNewItemButton.setBackground(Constants.Colors.ButtonsColor);
        addNewItemButton.setFont(Constants.Fonts.ButtonsFont);
        adminActionsPanel.add(addNewItemButton);

        editItemButton = new JButton("Edit Existing Item");
        editItemButton.setBounds(300, 0, 300, 50);
        editItemButton.setBackground(Constants.Colors.ButtonsColor);
        editItemButton.setFont(Constants.Fonts.ButtonsFont);
        adminActionsPanel.add(editItemButton);

        deleteItemButton = new JButton("Delete Existing Item");
        deleteItemButton.setBounds(600, 0, 300, 50);
        deleteItemButton.setBackground(Constants.Colors.ButtonsColor);
        deleteItemButton.setFont(Constants.Fonts.ButtonsFont);
        adminActionsPanel.add(deleteItemButton);

        waiterActionsPanel = new JPanel();
        waiterActionsPanel.setBackground(Constants.Colors.BackgroundColor);
        waiterActionsPanel.setBounds(0, 0, Constants.Frames.WIDTH, 50);
        waiterActionsPanel.setLayout(null);
        waiterActionsPanel.setVisible(false);
        contentPanel.add(waiterActionsPanel);

        createOrderButton = new JButton("New Order");
        createOrderButton.setBounds(0, 0, 300, 50);
        createOrderButton.setFont(Constants.Fonts.ButtonsFont);
        createOrderButton.setBackground(Constants.Colors.ButtonsColor);
        waiterActionsPanel.add(createOrderButton);

        computePriceButton = new JButton("Compute Price");
        computePriceButton.setBounds(300, 0, 300, 50);
        computePriceButton.setBackground(Constants.Colors.ButtonsColor);
        computePriceButton.setFont(Constants.Fonts.ButtonsFont);
        waiterActionsPanel.add(computePriceButton);

        generateBillButton = new JButton("Generate Bill");
        generateBillButton.setBounds(600, 0, 300, 50);
        generateBillButton.setBackground(Constants.Colors.ButtonsColor);
        generateBillButton.setFont(Constants.Fonts.ButtonsFont);
        waiterActionsPanel.add(generateBillButton);

        menuLabel = new JLabel("Menu");
        menuLabel.setFont(Constants.Fonts.LabelsFont);
        menuLabel.setForeground(Constants.Colors.BLACK);
        menuLabel.setHorizontalAlignment(JLabel.CENTER);
        menuLabel.setVerticalAlignment(JLabel.CENTER);
        menuLabel.setBounds(200, 75, 500, 40);
        menuLabel.setVisible(false);
        contentPanel.add(menuLabel);

        orderLabel = new JLabel("Orders");
        orderLabel.setFont(Constants.Fonts.LabelsFont);
        orderLabel.setForeground(Constants.Colors.BLACK);
        orderLabel.setHorizontalAlignment(JLabel.CENTER);
        orderLabel.setVerticalAlignment(JLabel.CENTER);
        orderLabel.setBounds(200, 75, 500, 40);
        orderLabel.setVisible(false);
        contentPanel.add(orderLabel);

        toCookLabel = new JLabel("New Order");
        toCookLabel.setFont(Constants.Fonts.LabelsFont);
        toCookLabel.setForeground(Constants.Colors.BLACK);
        toCookLabel.setHorizontalAlignment(JLabel.CENTER);
        toCookLabel.setVerticalAlignment(JLabel.CENTER);
        toCookLabel.setBounds(200, 75, 500, 40);
        toCookLabel.setVisible(false);
        contentPanel.add(toCookLabel);

        welcomeLabel = new JLabel("Welcome!");
        welcomeLabel.setFont(Constants.Fonts.LabelsFont);
        welcomeLabel.setForeground(Constants.Colors.TextColor);
        welcomeLabel.setHorizontalAlignment(JLabel.CENTER);
        welcomeLabel.setVerticalAlignment(JLabel.CENTER);
        welcomeLabel.setBounds(200, 200, 500, 40);
        welcomeLabel.setVisible(true);
        contentPanel.add(welcomeLabel);

        roleLabel = new JLabel("Please choose your role :)");
        roleLabel.setFont(Constants.Fonts.LabelsFont);
        roleLabel.setForeground(Constants.Colors.TextColor);
        roleLabel.setHorizontalAlignment(JLabel.CENTER);
        roleLabel.setVerticalAlignment(JLabel.CENTER);
        roleLabel.setBounds(200, 260, 500, 40);
        roleLabel.setVisible(true);
        contentPanel.add(roleLabel);

        noMenuItemsLabel = new JLabel("Unfortunately, our menu is empty...");
        noMenuItemsLabel.setFont(Constants.Fonts.LabelsFont);
        noMenuItemsLabel.setBounds(0, 200, Constants.Frames.WIDTH, 100);
        noMenuItemsLabel.setVerticalAlignment(JLabel.CENTER);
        noMenuItemsLabel.setHorizontalAlignment(JLabel.CENTER);
        noMenuItemsLabel.setVisible(false);
        contentPanel.add(noMenuItemsLabel);

        noOrdersLabel = new JLabel("There are no ordres now...");
        noOrdersLabel.setBounds(0, 200, Constants.Frames.WIDTH, 100);
        noOrdersLabel.setFont(Constants.Fonts.LabelsFont);
        noOrdersLabel.setHorizontalAlignment(JLabel.CENTER);
        noOrdersLabel.setVerticalAlignment(JLabel.CENTER);
        noOrdersLabel.setVisible(false);
        contentPanel.add(noOrdersLabel);

        menuScrollPane = new JScrollPane();
        contentPanel.add(menuScrollPane);

        orderScrollPane = new JScrollPane();
        contentPanel.add(orderScrollPane);

        chefScrollPane = new JScrollPane();
        chefTable = new JTable();
        chefScrollPane.add(chefTable);
        contentPanel.add(chefScrollPane);

        this.setVisible(true);
    }

    public static void displayMenuTable() {
        contentPanel.remove(menuScrollPane);

        defaultMenuTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        defaultMenuTableModel.setColumnIdentifiers(Constants.Tables.menuTableHeader);

        menuTable = new JTable(defaultMenuTableModel);
        menuTable.setBackground(Constants.Colors.TabelBackground);
        menuTable.getTableHeader().setFont(Constants.Fonts.TableFont);
        menuTable.getTableHeader().setBackground(Constants.Colors.TabelBackground);
        menuTable.getTableHeader().setPreferredSize(Constants.Tables.tableHeaderDimension);
        menuTable.setRowHeight(Constants.Tables.rowHeight);

        menuScrollPane = new JScrollPane(menuTable);
        menuScrollPane.setBackground(Constants.Colors.ToolbarColor);
        menuScrollPane.getVerticalScrollBar().setBackground(Constants.Colors.ToolbarColor);
        menuScrollPane.getVerticalScrollBar().setUI(new BasicScrollBarUI() {
            @Override
            protected void configureScrollBarColors() {
                this.thumbColor = Constants.Colors.ButtonsColor;
            }

            @Override
            protected JButton createDecreaseButton(int orientation) {
                JButton button = super.createDecreaseButton(orientation);
                button.setBackground(Constants.Colors.ButtonsColor);
                return button;
            }

            @Override
            protected JButton createIncreaseButton(int orientation) {
                JButton button = super.createIncreaseButton(orientation);
                button.setBackground(Constants.Colors.ButtonsColor);
                return button;
            }
        });

        if (restaurant.getMenuItems().size() * Constants.Tables.rowHeight + 38 <= Constants.Tables.scrollPaneMaximHeight ) {
            menuScrollPane.setBounds(120, 130, Constants.Tables.scrollPaneWidth, restaurant.getMenuItems().size() * Constants.Tables.rowHeight + 38);
        } else {
            menuScrollPane.setBounds(120, 130, Constants.Tables.scrollPaneWidth, Constants.Tables.scrollPaneMaximHeight);
        }
        menuTable.setBackground(Constants.Colors.BackgroundColor);
        menuScrollPane.setVisible(false);
        contentPanel.add(menuScrollPane);

        Object[] row = new Object[3];
        for (MenuItem menuItem : restaurant.getMenuItems()) {
            row[0] = menuItem.getName();
            if (menuItem instanceof BaseProduct) {
                row[1] = "Base Product";
            } else {
                row[1] = "Composite Product";
            }
            row[2] = menuItem.getPrice();
            defaultMenuTableModel.addRow(row);
        }

        if (restaurant.getMenuItems().size() > 0) {
            noMenuItemsLabel.setVisible(false);
            menuLabel.setVisible(true);
            menuScrollPane.setVisible(true);
        } else {
            noMenuItemsLabel.setVisible(true);
            menuLabel.setVisible(false);
            menuScrollPane.setVisible(false);
        }
    }

    public static void displayOrderTable() {
        contentPanel.remove(orderScrollPane);

        defaultOrderTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        defaultOrderTableModel.setColumnIdentifiers(Constants.Tables.orderTableHeader);

        orderTable = new JTable(defaultOrderTableModel);
        orderTable.setBackground(Constants.Colors.BackgroundColor);
        orderTable.getTableHeader().setFont(Constants.Fonts.TableFont);
        orderTable.getTableHeader().setBackground(Constants.Colors.TabelBackground);
        orderTable.getTableHeader().setPreferredSize(Constants.Tables.tableHeaderDimension);
        orderTable.setRowHeight(Constants.Tables.rowHeight);
        orderTable.getColumnModel().getColumn(2).setPreferredWidth(170);

        orderScrollPane = new JScrollPane(orderTable);
        orderScrollPane.setBackground(Constants.Colors.ToolbarColor);
        orderScrollPane.getVerticalScrollBar().setBackground(Constants.Colors.ToolbarColor);
        orderScrollPane.getVerticalScrollBar().setUI(new BasicScrollBarUI() {
            @Override
            protected void configureScrollBarColors() {
                this.thumbColor = Constants.Colors.ButtonsColor;
            }

            @Override
            protected JButton createDecreaseButton(int orientation) {
                JButton button = super.createDecreaseButton(orientation);
                button.setBackground(Constants.Colors.ButtonsColor);
                return button;
            }

            @Override
            protected JButton createIncreaseButton(int orientation) {
                JButton button = super.createIncreaseButton(orientation);
                button.setBackground(Constants.Colors.ButtonsColor);
                return button;
            }
        });

        if (restaurant.getOrders().size() * Constants.Tables.rowHeight + 38 <= Constants.Tables.scrollPaneMaximHeight ) {
            orderScrollPane.setBounds(120, 130, Constants.Tables.scrollPaneWidth, restaurant.getOrders().size() * Constants.Tables.rowHeight + 38);
        } else {
            orderScrollPane.setBounds(120, 130, Constants.Tables.scrollPaneWidth, Constants.Tables.scrollPaneMaximHeight);
        }
        orderScrollPane.setBackground(Constants.Colors.BackgroundColor);
        orderScrollPane.setVisible(false);
        contentPanel.add(orderScrollPane);

        Object[] row = new Object[4];
        for (Order order : restaurant.getOrders().keySet()) {
            row[0] = order.getId();
            row[1] = order.getTable();
            row[2] = order.getDate();
            if (order.getPrice() == 0) {
                row[3] = "-";
            } else {
                row[3] = order.getPrice();
            }
            defaultOrderTableModel.addRow(row);
        }

        if (restaurant.getOrders().size() > 0) {
            orderLabel.setVisible(true);
            noOrdersLabel.setVisible(false);
            orderScrollPane.setVisible(true);
        } else {
            noOrdersLabel.setVisible(true);
            orderLabel.setVisible(false);
            orderScrollPane.setVisible(false);
        }
    }

    public static void displayChefTable() {
        contentPanel.remove(chefTable);
        contentPanel.remove(chefScrollPane);

        defaultChefTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        defaultChefTableModel.setColumnIdentifiers(Constants.Tables.chefTableHeader);

        chefTable = new JTable(defaultChefTableModel);
        chefTable.setBackground(Constants.Colors.BackgroundColor);
        chefTable.getTableHeader().setFont(Constants.Fonts.TableFont);
        chefTable.getTableHeader().setBackground(Constants.Colors.TabelBackground);
        chefTable.getTableHeader().setPreferredSize(Constants.Tables.tableHeaderDimension);
        chefTable.setRowHeight(Constants.Tables.rowHeight);

        chefScrollPane = new JScrollPane(chefTable);
        chefScrollPane.setBackground(Constants.Colors.ToolbarColor);
        chefScrollPane.getVerticalScrollBar().setBackground(Constants.Colors.ToolbarColor);
        chefScrollPane.getVerticalScrollBar().setUI(new BasicScrollBarUI() {
            @Override
            protected void configureScrollBarColors() {
                this.thumbColor = Constants.Colors.ButtonsColor;
            }

            @Override
            protected JButton createDecreaseButton(int orientation) {
                JButton button = super.createDecreaseButton(orientation);
                button.setBackground(Constants.Colors.ButtonsColor);
                return button;
            }

            @Override
            protected JButton createIncreaseButton(int orientation) {
                JButton button = super.createIncreaseButton(orientation);
                button.setBackground(Constants.Colors.ButtonsColor);
                return button;
            }
        });

        Map<MenuItem, Integer> toChefMap = FileWritter.completeMap(toChef);
        if (toChefMap.size() * Constants.Tables.rowHeight + 38 <= Constants.Tables.scrollPaneMaximHeight ) {
            chefScrollPane.setBounds(120, 130, Constants.Tables.scrollPaneWidth, toChefMap.size() * Constants.Tables.rowHeight + 38);
        } else {
            chefScrollPane.setBounds(120, 130, Constants.Tables.scrollPaneWidth, Constants.Tables.scrollPaneMaximHeight);
       }
        chefScrollPane.setBackground(Constants.Colors.BackgroundColor);
        chefScrollPane.setVisible(false);
        contentPanel.add(chefScrollPane);

        Object[] row = new Object[4];
        for (MenuItem menuItem : toChefMap.keySet()) {
            row[0] = menuItem.getName();
            if (menuItem instanceof BaseProduct) {
                row[1] = "Base Product";
            } else {
                row[1] = "Composite Product";
            }
            row[2] = toChefMap.get(menuItem);
            defaultChefTableModel.addRow(row);
        }

        if (toChef.size() > 0) {
            noOrdersLabel.setVisible(false);
            toCookLabel.setVisible(true);
            chefScrollPane.setVisible(true);
        } else {
            noOrdersLabel.setVisible(true);
            toCookLabel.setVisible(false);
            chefScrollPane.setVisible(false);
        }
    }

    public static void deleteMenuTable() {
        menuScrollPane.setVisible(false);
        menuScrollPane.remove(menuTable);
        contentPanel.remove(menuScrollPane);
    }

    public static void deleteOrderTable() {
        orderScrollPane.setVisible(false);
        orderScrollPane.remove(orderTable);
        contentPanel.remove(orderScrollPane);
    }

    public static void deleteChefTable() {
        chefScrollPane.setVisible(false);
        chefScrollPane.remove(chefTable);
        contentPanel.remove(chefScrollPane);
    }

    public static Restaurant getRestaurant() {
        return restaurant;
    }

    public static void setRestaurant(Restaurant newRestaurant) {
        restaurant = newRestaurant;
    }

    public static JButton getAdminButton() {
        return adminButton;
    }

    public static JButton getWaiterButton() {
        return waiterButton;
    }

    public static JButton getChefButton() {
        return chefButton;
    }

    public static JButton getHomePageButton() {
        return homePageButton;
    }

    public static JPanel getAdminActionsPanel() {
        return adminActionsPanel;
    }

    public static JPanel getWaiterActionsPanel() {
        return waiterActionsPanel;
    }

    public static JScrollPane getMenuScrollPane() {
        return menuScrollPane;
    }

    public static JScrollPane getOrderScrollPane() {
        return orderScrollPane;
    }

    public static JScrollPane getChefScrollPane() {
        return chefScrollPane;
    }

    public static DefaultTableModel getDefaultMenuTableModel() {
        return defaultMenuTableModel;
    }

    public static DefaultTableModel getDefaultOrderTableModel() {
        return defaultOrderTableModel;
    }

    public static JTable getMenuTable() {
        return menuTable;
    }

    public static JTable getOrderTable() {
        return orderTable;
    }

    public static JLabel getMenuLabel() {
        return menuLabel;
    }

    public static JLabel getOrderLabel() {
        return orderLabel;
    }

    public static JLabel getToCookLabel() {
        return toCookLabel;
    }

    public static JLabel getWelcomeLabel() {
        return welcomeLabel;
    }

    public static JLabel getRoleLabel() {
        return roleLabel;
    }

    public static JLabel getNoMenuItemsLabel() {
        return noMenuItemsLabel;
    }

    public static JLabel getNoOrdersLabel() {
        return noOrdersLabel;
    }

    public void setAdminButtonActionListener(ActionListener actionListener) {
        adminButton.addActionListener(actionListener);
    }

    public void setWaiterButtonActionListener(ActionListener actionListener) {
        waiterButton.addActionListener(actionListener);
    }

    public void setChefButtonActionListener(ActionListener actionListener) {
        chefButton.addActionListener(actionListener);
    }

    public void setHomePageButtonActionListener(ActionListener actionListener) {
        homePageButton.addActionListener(actionListener);
    }

    public void setAddNewItemButtonActionListener(ActionListener actionListener) {
        addNewItemButton.addActionListener(actionListener);
    }

    public void setEditItemButtonActionListener(ActionListener actionListener) {
        editItemButton.addActionListener(actionListener);
    }

    public void setDeleteItemButtonActionListener(ActionListener actionListener) {
        deleteItemButton.addActionListener(actionListener);
    }

    public void setCreateOrderButtonActionlistener(ActionListener actionlistener) {
        createOrderButton.addActionListener(actionlistener);
    }

    public void setComputePriceButtonActionListener(ActionListener actionListener) {
        computePriceButton.addActionListener(actionListener);
    }

    public void setGenerateBillButtonActionListener(ActionListener actionListener) {
        generateBillButton.addActionListener(actionListener);
    }

    @Override
    public void update(Observable o, Object arg) {
        toChef = (ArrayList<MenuItem>) arg;
    }
}
