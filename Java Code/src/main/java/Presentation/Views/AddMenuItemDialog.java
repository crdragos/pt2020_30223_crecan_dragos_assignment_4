package Presentation.Views;

import BLL.BaseProduct;
import BLL.CompositeProduct;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

public class AddMenuItemDialog extends AbstractDialog {

    private JLabel baseProductLabel;
    private JLabel compositeProductLabel;

    private static JLabel nameLabel;
    private static JLabel priceLabel;

    private static JRadioButton baseProductButton;
    private static JRadioButton compositeProductButton;

    private static JTextField nameTextField;
    private static JTextField priceTextField;

    private static JButton componentsButton;

    public AddMenuItemDialog() {
        super();

        this.titleLabel.setText("Add New Menu Item");

        baseProductLabel = new JLabel("Base product");
        baseProductLabel.setFont(Constants.Fonts.LabelsFont);
        baseProductLabel.setHorizontalAlignment(JLabel.LEFT);
        baseProductLabel.setVerticalAlignment(JLabel.CENTER);
        baseProductLabel.setBounds(25, 75, 250, 50);
        this.contentPanel.add(baseProductLabel);

        baseProductButton = new JRadioButton();
        baseProductButton.setBackground(Constants.Colors.BackgroundColor);
        baseProductButton.setBounds(280, 95, 20, 20);
        this.contentPanel.add(baseProductButton);

        compositeProductLabel = new JLabel("Composite product");
        compositeProductLabel.setFont(Constants.Fonts.LabelsFont);
        compositeProductLabel.setHorizontalAlignment(JLabel.LEFT);
        compositeProductLabel.setVerticalAlignment(JLabel.CENTER);
        compositeProductLabel.setBounds(25, 130, 250, 50);
        this.contentPanel.add(compositeProductLabel);

        compositeProductButton = new JRadioButton();
        compositeProductButton.setBackground(Constants.Colors.BackgroundColor);
        compositeProductButton.setBounds(280, 150, 20, 20);
        this.contentPanel.add(compositeProductButton);

        nameLabel = new JLabel("Name :");
        nameLabel.setFont(Constants.Fonts.LabelsFont);
        nameLabel.setHorizontalAlignment(JLabel.LEFT);
        nameLabel.setVerticalAlignment(JLabel.CENTER);
        nameLabel.setBounds(25, 185, 150, 50);
        this.contentPanel.add(nameLabel);

        nameTextField = new JTextField();
        nameTextField.setHorizontalAlignment(JTextField.CENTER);
        nameTextField.setFont(Constants.Fonts.TextFiledsFont);
        nameTextField.setBounds(180, 200, 400, 30);
        this.contentPanel.add(nameTextField);

        priceLabel = new JLabel("Price :");
        priceLabel.setFont(Constants.Fonts.LabelsFont);
        priceLabel.setHorizontalAlignment(JLabel.LEFT);
        priceLabel.setVerticalAlignment(JLabel.CENTER);
        priceLabel.setBounds(25, 240, 150, 50);
        priceLabel.setVisible(false);
        this.contentPanel.add(priceLabel);

        priceTextField = new JTextField();
        priceTextField.setHorizontalAlignment(JTextField.CENTER);
        priceTextField.setFont(Constants.Fonts.TextFiledsFont);
        priceTextField.setBounds(180, 255, 400, 30);
        priceTextField.setVisible(false);
        this.contentPanel.add(priceTextField);

        componentsButton = new JButton("Add Components");
        componentsButton.setBackground(Constants.Colors.ButtonsColor);
        componentsButton.setFont(Constants.Fonts.ButtonsFont);
        componentsButton.setBounds(240, 255, 200, 30);
        componentsButton.setVisible(false);
        this.contentPanel.add(componentsButton);

        this.confirmButton.addActionListener(e -> {
            if (!AddMenuItemDialog.getBaseProductButton().isSelected() && !AddMenuItemDialog.getCompositeProductButton().isSelected()) {
                new MessageDialog("Please select product type!");
            } else if (AddMenuItemDialog.getBaseProductButton().isSelected()) {
                if (AddMenuItemDialog.getNameTextField().getText().isEmpty() || AddMenuItemDialog.getPriceTextField().getText().isEmpty()) {
                    new MessageDialog("Please complete all the fields");
                } else {
                    String productName = AddMenuItemDialog.getNameTextField().getText();
                    if (Pattern.matches("[a-z -?A-z]+", productName)) {
                        try {
                            Float productPrice = Float.parseFloat(AddMenuItemDialog.getPriceTextField().getText());

                            if (RestaurantFrame.getRestaurant().search(productName) == null) {
                                RestaurantFrame.deleteMenuTable();
                                RestaurantFrame.getRestaurant().createMenuItem(new BaseProduct(productName, productPrice));
                                RestaurantFrame.displayMenuTable();
                            }
                            this.dispose();
                        } catch (NumberFormatException numberFormatException) {
                            new MessageDialog("The price must be a float number!");
                        }
                    } else {
                        new MessageDialog("Name must contain just letters!");
                    }
                }
            } else if (AddMenuItemDialog.getCompositeProductButton().isSelected()) {
                if (AddMenuItemDialog.getNameTextField().getText().isEmpty()) {
                    new MessageDialog("Please complete the name field");
                } else if (ComponentsDialog.getProductComponents().isEmpty()) {
                    new MessageDialog("Please select product components");
                } else {
                    String productName = AddMenuItemDialog.getNameTextField().getText();
                    RestaurantFrame.deleteMenuTable();
                    RestaurantFrame.getRestaurant().createMenuItem(new CompositeProduct(productName,ComponentsDialog.getProductComponents()));
                    RestaurantFrame.displayMenuTable();
                    this.dispose();
                }
            }
        });
    }

    public static JRadioButton getBaseProductButton() {
        return baseProductButton;
    }

    public static JRadioButton getCompositeProductButton() {
        return compositeProductButton;
    }

    public static JTextField getNameTextField() {
        return nameTextField;
    }

    public static JTextField getPriceTextField() {
        return priceTextField;
    }

    public static JLabel getPriceLabel() {
        return priceLabel;
    }

    public static JButton getComponentsButton() {
        return componentsButton;
    }

    public void setBaseProductButtonChangeListener(ChangeListener changeListener) {
        baseProductButton.addChangeListener(changeListener);
    }

    public void setCompositeProductButtonChangeListener(ChangeListener changeListener) {
        compositeProductButton.addChangeListener(changeListener);
    }

    public void setComponentsButtonActionListener(ActionListener actionListener) {
        componentsButton.addActionListener(actionListener);
    }
}
