package Presentation.Views;

import BLL.BaseProduct;
import BLL.MenuItem;
import BLL.Order;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;

public class NewOrderDialog extends JFrame {

    private JPanel contentPanel;
    private JPanel buttonsPannel;
    private JPanel tablePanel;

    private JButton addProductsButton;
    private JButton confirmButton;
    private JButton cancelButton;

    private JLabel titleLabel;

    private JLabel tableLabel;
    private JTextField tableTextField;

    private static DefaultTableModel defaultTableModel;
    private static JTable table;
    private static JScrollPane scrollPane;

    private HashSet<MenuItem> menuItems;
    private static ArrayList<MenuItem> orderedProducts;

    public NewOrderDialog() {
        this.setSize(Constants.Dialogs.NewOrderDialogWidth, Constants.Dialogs.NewOrderDialogHeight);
        this.setLayout(null);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setUndecorated(true);
        this.getRootPane().setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Constants.Colors.ToolbarColor));
        this.setResizable(false);
        this.setVisible(true);

        this.menuItems = RestaurantFrame.getRestaurant().getMenuItems();
        this.orderedProducts = new ArrayList<>();

        contentPanel = new JPanel();
        contentPanel.setBounds(0, 0, Constants.Dialogs.NewOrderDialogWidth, Constants.Dialogs.NewOrderDialogHeight);
        contentPanel.setLayout(null);
        contentPanel.setBackground(Constants.Colors.BackgroundColor);
        this.add(contentPanel);

        titleLabel = new JLabel("Create New Order");
        titleLabel.setBounds(0, 5, Constants.Dialogs.NewOrderDialogWidth, 40);
        titleLabel.setFont(Constants.Fonts.LabelsFont);
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setVerticalAlignment(JLabel.CENTER);
        titleLabel.setVisible(true);
        contentPanel.add(titleLabel);

        buttonsPannel = new JPanel();
        buttonsPannel.setBounds(0, 50, 300, 450);
        buttonsPannel.setLayout(null);
        buttonsPannel.setBackground(Constants.Colors.BackgroundColor);
        contentPanel.add(buttonsPannel);

        tableLabel = new JLabel("Table");
        tableLabel.setBounds(10, 55, 140, 30);
        tableLabel.setFont(Constants.Fonts.LabelsFont);
        tableLabel.setHorizontalAlignment(JLabel.CENTER);
        tableLabel.setVerticalAlignment(JLabel.CENTER);
        tableLabel.setVisible(true);
        buttonsPannel.add(tableLabel);

        tableTextField = new JTextField();
        tableTextField.setBounds(160, 55, 130, 30);
        tableTextField.setFont(Constants.Fonts.TextFiledsFont);
        tableTextField.setHorizontalAlignment(JTextField.CENTER);
        tableTextField.setVisible(true);
        buttonsPannel.add(tableTextField);

        addProductsButton = new JButton("Add Products");
        addProductsButton.setBounds(10, 120, 280, 40);
        addProductsButton.setBackground(Constants.Colors.ButtonsColor);
        addProductsButton.setFont(Constants.Fonts.ButtonsFont);
        addProductsButton.setHorizontalAlignment(JButton.CENTER);
        addProductsButton.setVerticalAlignment(JButton.CENTER);
        addProductsButton.setVisible(true);
        buttonsPannel.add(addProductsButton);

        confirmButton = new JButton("Confirm");
        confirmButton.setBounds(10, 300, 100, 40);
        confirmButton.setBackground(Constants.Colors.ButtonsColor);
        confirmButton.setFont(Constants.Fonts.ButtonsFont);
        confirmButton.setHorizontalAlignment(JButton.CENTER);
        confirmButton.setVerticalAlignment(JButton.CENTER);
        confirmButton.setVisible(true);
        confirmButton.addActionListener(e -> {
            if (tableTextField.getText().isEmpty()) {
                new MessageDialog("Please insert the table number!");
            } else if (orderedProducts.isEmpty()) {
                new MessageDialog("Please add products to order!");
            } else {
                try {
                    Integer tableNumber = Integer.parseInt(tableTextField.getText());
                    Order order = new Order(tableNumber);
                    RestaurantFrame.getRestaurant().createNewOrder(order, orderedProducts);
                    RestaurantFrame.deleteOrderTable();
                    RestaurantFrame.displayOrderTable();
                    this.dispose();
                } catch (NumberFormatException numberFormatException) {
                    new MessageDialog("Table must be an integer number!");
                }
            }
        });
        buttonsPannel.add(confirmButton);

        cancelButton = new JButton("Cancel");
        cancelButton.setBounds(190, 300, 100, 40);
        cancelButton.setBackground(Constants.Colors.BackgroundColor);
        cancelButton.setFont(Constants.Fonts.ButtonsFont);
        cancelButton.setHorizontalAlignment(JButton.CENTER);
        cancelButton.setVerticalAlignment(JButton.CENTER);
        cancelButton.setForeground(Color.RED);
        cancelButton.setBorderPainted(false);
        cancelButton.setVisible(true);
        cancelButton.addActionListener(e -> {
            this.dispose();
        });
        buttonsPannel.add(cancelButton);

        tablePanel = new JPanel();
        tablePanel.setBounds(300, 50, 500, 450);
        tablePanel.setLayout(null);
        tablePanel.setBackground(Constants.Colors.BackgroundColor);
        contentPanel.add(tablePanel);

        displayMenuTable();
    }

    public void displayMenuTable() {
        defaultTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        defaultTableModel.setColumnIdentifiers(Constants.Tables.menuTableHeader);

        table = new JTable(defaultTableModel);
        table.setBackground(Constants.Colors.TabelBackground);
        table.getTableHeader().setFont(Constants.Fonts.TableFont);
        table.getTableHeader().setBackground(Constants.Colors.TabelBackground);
        table.getTableHeader().setPreferredSize(Constants.Tables.tableHeaderDimension);
        table.setRowHeight(Constants.Tables.rowHeight);

        scrollPane = new JScrollPane(table);
        if (RestaurantFrame.getRestaurant().getMenuItems().size() * Constants.Tables.rowHeight + 38 <= 350 ) {
            scrollPane.setBounds(10, 55, 470, RestaurantFrame.getRestaurant().getMenuItems().size() * Constants.Tables.rowHeight + 38);
        } else {
            scrollPane.setBounds(10, 55, 470, 350);
        }
        table.setBackground(Constants.Colors.BackgroundColor);
        scrollPane.setVisible(true);
        tablePanel.add(scrollPane);

        Object[] row = new Object[3];
        for (MenuItem menuItem : menuItems) {
            row[0] = menuItem.getName();
            if (menuItem instanceof BaseProduct) {
                row[1] = "Base Product";
            } else {
                row[1] = "Composite Product";
            }
            row[2] = menuItem.getPrice();
            defaultTableModel.addRow(row);
        }
    }

    public static ArrayList<MenuItem> getOrderedProducts() {
        return orderedProducts;
    }

    public static JTable getTable() {
        return table;
    }

    public void setAddProductsButtonActionListener(ActionListener actionListener) {
        addProductsButton.addActionListener(actionListener);
    }
}
