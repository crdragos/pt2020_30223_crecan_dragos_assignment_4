package Presentation.Views;

import BLL.BaseProduct;
import BLL.MenuItem;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

public class EditDialog extends AbstractDialog {

    private JLabel baseProductLabel;
    private JLabel compositeProductLabel;
    private JLabel nameLabel;
    private static JLabel priceLabel;

    private static JRadioButton baseProductButton;
    private static JRadioButton compositeProductButton;

    private static JTextField nameTextField;
    private static JTextField priceTextField;

    private static JButton addComponentsButton;
    private static JButton removeComponentsButton;

    public EditDialog() {
        super();

        this.titleLabel.setText("Edit Existing Item");

        baseProductLabel = new JLabel("Base product");
        baseProductLabel.setFont(Constants.Fonts.LabelsFont);
        baseProductLabel.setHorizontalAlignment(JLabel.LEFT);
        baseProductLabel.setVerticalAlignment(JLabel.CENTER);
        baseProductLabel.setBounds(25, 75, 250, 50);
        this.contentPanel.add(baseProductLabel);

        compositeProductLabel = new JLabel("Composite product");
        compositeProductLabel.setFont(Constants.Fonts.LabelsFont);
        compositeProductLabel.setHorizontalAlignment(JLabel.LEFT);
        compositeProductLabel.setVerticalAlignment(JLabel.CENTER);
        compositeProductLabel.setBounds(25, 130, 250, 50);
        this.contentPanel.add(compositeProductLabel);

        baseProductButton = new JRadioButton();
        baseProductButton.setBackground(Constants.Colors.BackgroundColor);
        baseProductButton.setBounds(280, 95, 20, 20);
        this.contentPanel.add(baseProductButton);

        compositeProductButton = new JRadioButton();
        compositeProductButton.setBackground(Constants.Colors.BackgroundColor);
        compositeProductButton.setBounds(280, 150, 20, 20);
        this.contentPanel.add(compositeProductButton);

        String productType = String.valueOf(RestaurantFrame.getMenuTable().getModel().getValueAt(RestaurantFrame.getMenuTable().getSelectedRow(), 1));
        if (productType.equals("Base Product")) {
            baseProductButton.setSelected(true);
        } else {
            compositeProductButton.setSelected(true);
        }

        nameLabel = new JLabel("Name :");
        nameLabel.setFont(Constants.Fonts.LabelsFont);
        nameLabel.setHorizontalAlignment(JLabel.LEFT);
        nameLabel.setVerticalAlignment(JLabel.CENTER);
        nameLabel.setBounds(25, 185, 150, 50);
        this.contentPanel.add(nameLabel);

        nameTextField = new JTextField();
        nameTextField.setHorizontalAlignment(JTextField.CENTER);
        nameTextField.setFont(Constants.Fonts.TextFiledsFont);
        nameTextField.setText(String.valueOf(RestaurantFrame.getMenuTable().getModel().getValueAt(RestaurantFrame.getMenuTable().getSelectedRow(), 0)));
        nameTextField.setBounds(180, 200, 400, 30);
        if (compositeProductButton.isSelected()) {
            nameTextField.setEditable(false);
        }
        this.contentPanel.add(nameTextField);

        priceLabel = new JLabel("Price :");
        priceLabel.setFont(Constants.Fonts.LabelsFont);
        priceLabel.setHorizontalAlignment(JLabel.LEFT);
        priceLabel.setVerticalAlignment(JLabel.CENTER);
        priceLabel.setBounds(25, 240, 150, 50);
        if (baseProductButton.isSelected()) {
            priceLabel.setVisible(true);
        } else {
            priceLabel.setVisible(false);
        }
        this.contentPanel.add(priceLabel);

        priceTextField = new JTextField();
        priceTextField.setHorizontalAlignment(JTextField.CENTER);
        priceTextField.setFont(Constants.Fonts.TextFiledsFont);
        priceTextField.setText(String.valueOf(RestaurantFrame.getMenuTable().getModel().getValueAt(RestaurantFrame.getMenuTable().getSelectedRow(), 2)));
        priceTextField.setBounds(180, 255, 400, 30);
        if (baseProductButton.isSelected()) {
            priceTextField.setVisible(true);
        } else {
            priceTextField.setVisible(false);
        }
        this.contentPanel.add(priceTextField);

        addComponentsButton = new JButton("Add Components");
        addComponentsButton.setBounds(75, 255, 200, 30);
        addComponentsButton.setBackground(Constants.Colors.ButtonsColor);
        addComponentsButton.setFont(Constants.Fonts.ButtonsFont);
        addComponentsButton.setHorizontalAlignment(JButton.CENTER);
        addComponentsButton.setVerticalAlignment(JButton.CENTER);
        addComponentsButton.setVisible(false);
        if (compositeProductButton.isSelected()) {
            addComponentsButton.setVisible(true);
        } else {
            addComponentsButton.setVisible(false);
        }
        this.contentPanel.add(addComponentsButton);

        removeComponentsButton = new JButton("Remove Components");
        removeComponentsButton.setBounds(370, 255, 200, 30);
        removeComponentsButton.setBackground(Constants.Colors.ButtonsColor);
        removeComponentsButton.setFont(Constants.Fonts.ButtonsFont);
        removeComponentsButton.setHorizontalAlignment(JButton.CENTER);
        removeComponentsButton.setVerticalAlignment(JButton.CENTER);
        if (compositeProductButton.isSelected()) {
            removeComponentsButton.setVisible(true);
        } else {
            removeComponentsButton.setVisible(false);
        }
        this.contentPanel.add(removeComponentsButton);


        this.confirmButton.addActionListener(e -> {
            if (!EditDialog.getBaseProductButton().isSelected() && !EditDialog.getCompositeProductButton().isSelected()) {
                new MessageDialog("Please selcect product type!");
            } else if (EditDialog.getBaseProductButton().isSelected()) {
                if (EditDialog.getNameTextField().getText().isEmpty() || EditDialog.getPriceTextField().getText().isEmpty()) {
                    new MessageDialog("Please complete all the fields!");
                } else {
                    String previosName = String.valueOf(RestaurantFrame.getMenuTable().getModel().getValueAt(RestaurantFrame.getMenuTable().getSelectedRow(), 0));
                    MenuItem oldItem = RestaurantFrame.getRestaurant().search(previosName);
                    String newName = EditDialog.getNameTextField().getText();
                    if (Pattern.matches("[a-z -?A-z]+", newName)) {
                        try {
                            Float newPrice = Float.valueOf(EditDialog.getPriceTextField().getText());
                            MenuItem newItem = new BaseProduct(newName, newPrice);
                            RestaurantFrame.getRestaurant().editMenuItem(oldItem, newItem);
                            this.dispose();
                        } catch (NumberFormatException numberFormatException) {
                            new MessageDialog("The price must be a float number!");
                        }
                    } else {
                        new MessageDialog("Name must contain just letters!");
                    }
                    RestaurantFrame.deleteMenuTable();
                    RestaurantFrame.displayMenuTable();
                    this.dispose();
                }
            } else if (EditDialog.getCompositeProductButton().isSelected()) {
                if (EditDialog.getNameTextField().getText().isEmpty()) {
                    new MessageDialog("Please complete the name field!");
                } else {
                    RestaurantFrame.deleteMenuTable();
                    RestaurantFrame.displayMenuTable();
                    this.dispose();
                }
            }
        });
    }

    public static JRadioButton getBaseProductButton() {
        return baseProductButton;
    }

    public static JRadioButton getCompositeProductButton() {
        return compositeProductButton;
    }

    public static JTextField getNameTextField() {
        return nameTextField;
    }

    public static JLabel getPriceLabel() {
        return priceLabel;
    }

    public static JTextField getPriceTextField() {
        return priceTextField;
    }

    public static JButton getAddComponentsButton() {
        return addComponentsButton;
    }

    public static JButton getRemoveComponentsButton() {
        return  removeComponentsButton;
    }

    public void setBaseProductButtonChangeListener(ChangeListener changeListener) {
        baseProductButton.addChangeListener(changeListener);
    }

    public void setCompositeProductButtonChanegListener(ChangeListener chanegListener) {
        compositeProductButton.addChangeListener(chanegListener);
    }

    public void setAddComponentsButtonActionListener(ActionListener actionListener) {
        addComponentsButton.addActionListener(actionListener);
    }

    public void setRemoveComponentsButtonActionListener(ActionListener actionListener) {
        removeComponentsButton.addActionListener(actionListener);
    }
}
