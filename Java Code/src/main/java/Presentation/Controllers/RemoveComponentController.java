package Presentation.Controllers;

import BLL.MenuItem;
import Presentation.Views.MessageDialog;
import Presentation.Views.RemoveComponentDialog;
import Presentation.Views.RestaurantFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RemoveComponentController {

    public RemoveComponentController(RemoveComponentDialog removeComponentDialog) {
        removeComponentDialog.setRemoveComponentsButton(new RemoveButtonActionListenr());
    }

    private class RemoveButtonActionListenr implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            MenuItem oldItem = RestaurantFrame.getRestaurant().search(String.valueOf(RestaurantFrame.getMenuTable().getModel().getValueAt(RestaurantFrame.getMenuTable().getSelectedRow(), 0)));
            int[] selectedRows = RemoveComponentDialog.getTable().getSelectedRows();
            for (int rowIndex = selectedRows.length - 1; rowIndex >= 0; rowIndex--) {
                MenuItem toRemoveItem = RestaurantFrame.getRestaurant().search(String.valueOf(RemoveComponentDialog.getTable().getModel().getValueAt(selectedRows[rowIndex], 0)));
                RemoveComponentDialog.getToRemoveComponents().add(toRemoveItem);
                if (RemoveComponentDialog.getToRemoveComponents().size() == oldItem.getComponents().size() || RemoveComponentDialog.getToRemoveComponents().size() == oldItem.getComponents().size() - 1) {
                    new MessageDialog("A composite product must contain at least 2 products!");
                    RemoveComponentDialog.getToRemoveComponents().remove(toRemoveItem);
                } else {
                    System.out.println(RemoveComponentDialog.getToRemoveComponents().size());
                    RemoveComponentDialog.getDefaultTableModel().removeRow(selectedRows[rowIndex]);
                    RemoveComponentDialog.getPriceTextField().setText(String.valueOf(Float.parseFloat(RemoveComponentDialog.getPriceTextField().getText()) - toRemoveItem.getPrice()));
                }
            }
        }
    }
}
