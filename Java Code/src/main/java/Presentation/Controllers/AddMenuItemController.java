package Presentation.Controllers;

import Presentation.Views.AddMenuItemDialog;
import Presentation.Views.ComponentsDialog;
import Presentation.Views.RestaurantFrame;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddMenuItemController {

    public AddMenuItemController(AddMenuItemDialog addMenuItemDialog) {
        addMenuItemDialog.setBaseProductButtonChangeListener(new BaseProductButtonChangeListener());
        addMenuItemDialog.setCompositeProductButtonChangeListener(new CompositeProductButtonChangeListener());
        addMenuItemDialog.setComponentsButtonActionListener(new ComponentesButtonActionListener());
    }

    private class BaseProductButtonChangeListener implements ChangeListener {

        @Override
        public void stateChanged(ChangeEvent e) {
            if (AddMenuItemDialog.getBaseProductButton().isSelected()) {
                AddMenuItemDialog.getCompositeProductButton().setSelected(false);
                AddMenuItemDialog.getPriceTextField().setVisible(true);
                AddMenuItemDialog.getPriceLabel().setVisible(true);
            } else {
                AddMenuItemDialog.getPriceLabel().setVisible(false);
                AddMenuItemDialog.getPriceTextField().setVisible(false);
            }
        }
    }

    private class CompositeProductButtonChangeListener implements ChangeListener {

        @Override
        public void stateChanged(ChangeEvent e) {
            if (AddMenuItemDialog.getCompositeProductButton().isSelected()) {
                AddMenuItemDialog.getBaseProductButton().setSelected(false);
                AddMenuItemDialog.getComponentsButton().setVisible(true);
            } else {
                AddMenuItemDialog.getComponentsButton().setVisible(false);
            }
        }
    }

    private class ComponentesButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            RestaurantFrame.deleteMenuTable();
            RestaurantFrame.displayMenuTable();
            new ComponentsController(new ComponentsDialog(RestaurantFrame.getDefaultMenuTableModel(), RestaurantFrame.getMenuTable(), RestaurantFrame.getMenuScrollPane()));
        }
    }
}
