package Presentation.Controllers;

import BLL.MenuItem;
import Presentation.Views.AddComponentDialog;
import Presentation.Views.EditDialog;
import Presentation.Views.MessageDialog;
import Presentation.Views.RestaurantFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddComponentController {

    public AddComponentController(AddComponentDialog addComponentDialog) {
        addComponentDialog.setAddComponentsButton(new AddComponentsButtonActionListener());
    }

    private class AddComponentsButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            int[] selectedRows = AddComponentDialog.getTable().getSelectedRows();
            for (Integer rowIndex : selectedRows) {
                MenuItem newComponent = RestaurantFrame.getRestaurant().search(String.valueOf(AddComponentDialog.getTable().getModel().getValueAt(rowIndex, 0)));
                if (RestaurantFrame.getRestaurant().search(AddComponentDialog.getOldProductName()).getComponents().contains(newComponent)) {
                    new MessageDialog("Product " + newComponent.getName() + " already added!");
                } else if (String.valueOf(RestaurantFrame.getMenuTable().getModel().getValueAt(RestaurantFrame.getMenuTable().getSelectedRow(), 0)).equals(newComponent.getName())) {
                    new MessageDialog(newComponent.getName() + " is acctually the product!");
                } else {
                    AddComponentDialog.getPriceTextField().setText(String.valueOf(Float.parseFloat(AddComponentDialog.getPriceTextField().getText()) + newComponent.getPrice()));
                    AddComponentDialog.getNewComponents().add(newComponent);
                }
            }
        }
    }
}
