package Presentation.Controllers;

import BLL.MenuItem;
import Presentation.Views.DeleteDialog;
import Presentation.Views.RestaurantFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DeleteItemController {

    public DeleteItemController(DeleteDialog deleteDialog) {
        deleteDialog.setConfirmButtonActionListener(new ConfirmButtonActionListener());

    }

    private class ConfirmButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            RestaurantFrame.deleteMenuTable();


            String productName = String.valueOf(RestaurantFrame.getMenuTable().getModel().getValueAt(RestaurantFrame.getMenuTable().getSelectedRow(), 0));
            MenuItem menuItem1 = RestaurantFrame.getRestaurant().search(productName);
            RestaurantFrame.getRestaurant().deleteMenuItem(menuItem1);

            RestaurantFrame.displayMenuTable();
        }
    }
}
