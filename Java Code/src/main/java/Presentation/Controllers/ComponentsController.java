package Presentation.Controllers;

import Presentation.Views.ComponentsDialog;
import Presentation.Views.RestaurantFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ComponentsController {

    public ComponentsController(ComponentsDialog componentsDialog) {
        componentsDialog.setAddButtonActionListener(new AddButtonActionListener());
    }

    private class DoneButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            RestaurantFrame.deleteMenuTable();
            RestaurantFrame.displayMenuTable();
        }
    }

    private class AddButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            int[] selectedRows = ComponentsDialog.getTable().getSelectedRows();
            for (int i = 0; i < selectedRows.length; i++) {
                String componentName = String.valueOf(ComponentsDialog.getTable().getModel().getValueAt(selectedRows[i], 0));
                ComponentsDialog.getProductComponents().add(RestaurantFrame.getRestaurant().search(componentName));
            }
        }
    }
}
