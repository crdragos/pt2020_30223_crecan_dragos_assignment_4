package Presentation.Controllers;

import Presentation.Views.NewOrderDialog;
import Presentation.Views.RestaurantFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NewOrderController {

    public NewOrderController(NewOrderDialog newOrderDialog) {
        newOrderDialog.setAddProductsButtonActionListener(new AddProductsButtonActionListener());
    }

    private class AddProductsButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            int[] selectedRows = NewOrderDialog.getTable().getSelectedRows();
            for (Integer rowIndex : selectedRows) {
                String productName = String.valueOf(NewOrderDialog.getTable().getModel().getValueAt(rowIndex, 0));
                NewOrderDialog.getOrderedProducts().add(RestaurantFrame.getRestaurant().search(productName));
            }
        }
    }
}
