package Presentation.Controllers;

import Presentation.Views.AddComponentDialog;
import Presentation.Views.EditDialog;
import Presentation.Views.RemoveComponentDialog;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EditExistingItemController {

    public EditExistingItemController(EditDialog editDialog) {
        editDialog.setBaseProductButtonChangeListener(new BaseProductButtonChangeListener());
        editDialog.setCompositeProductButtonChanegListener(new CompositeProductButtonChangeListener());
        editDialog.setAddComponentsButtonActionListener(new AddButtonActionLister());
        editDialog.setRemoveComponentsButtonActionListener(new RemoveButtonActionListener());
    }

    private class BaseProductButtonChangeListener implements ChangeListener {

        @Override
        public void stateChanged(ChangeEvent e) {
            if (EditDialog.getBaseProductButton().isSelected()) {
                EditDialog.getCompositeProductButton().setSelected(false);
                EditDialog.getPriceLabel().setVisible(true);
                EditDialog.getPriceTextField().setVisible(true);
            } else {
                EditDialog.getPriceLabel().setVisible(false);
                EditDialog.getPriceTextField().setVisible(false);
            }
        }
    }

    private class CompositeProductButtonChangeListener implements ChangeListener {

        @Override
        public void stateChanged(ChangeEvent e) {
            if (EditDialog.getCompositeProductButton().isSelected()) {
                EditDialog.getBaseProductButton().setSelected(false);
                EditDialog.getAddComponentsButton().setVisible(true);
                EditDialog.getRemoveComponentsButton().setVisible(true);
            } else {
                EditDialog.getAddComponentsButton().setVisible(false);
                EditDialog.getRemoveComponentsButton().setVisible(false);
            }
        }
    }

    private class AddButtonActionLister implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            new AddComponentController(new AddComponentDialog());
        }
    }

    public class RemoveButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            new RemoveComponentController(new RemoveComponentDialog());
        }
    }
}
