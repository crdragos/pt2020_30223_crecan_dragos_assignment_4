package Presentation.Controllers;

import BLL.Order;
import BLL.Restaurant;
import Presentation.Views.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RestaurantController {

    public static RestaurantFrame restaurantFrame;

    public RestaurantController(RestaurantFrame restaurantFrame) {
        RestaurantController.restaurantFrame = restaurantFrame;
        RestaurantFrame.setRestaurant(new Restaurant());
        restaurantFrame.setAdminButtonActionListener(new AdminButtonActionListener());
        restaurantFrame.setWaiterButtonActionListener(new WaiterButtonActionListener());
        restaurantFrame.setChefButtonActionListener(new ChefButtonActionLisneter());
        restaurantFrame.setHomePageButtonActionListener(new HomePageActionListener());
        restaurantFrame.setAddNewItemButtonActionListener(new NewItemButtonActionListener());
        restaurantFrame.setEditItemButtonActionListener(new EditExistingItemButtonActionListener());
        restaurantFrame.setDeleteItemButtonActionListener(new DeleteItemActionListener());
        restaurantFrame.setCreateOrderButtonActionlistener(new CreateOrderButtonActionListener());
        restaurantFrame.setComputePriceButtonActionListener(new ComputePriceButtonActionListener());
        restaurantFrame.setGenerateBillButtonActionListener(new GenerateBillActionListener());
    }

    private class AdminButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            RestaurantFrame.getAdminButton().setBackground(Constants.Colors.ButtonsColor);
            RestaurantFrame.getWaiterButton().setBackground(Constants.Colors.ToolbarColor);
            RestaurantFrame.getChefButton().setBackground(Constants.Colors.ToolbarColor);
            RestaurantFrame.getHomePageButton().setBackground(Constants.Colors.ToolbarColor);

            RestaurantFrame.getAdminActionsPanel().setVisible(true);
            RestaurantFrame.getWaiterActionsPanel().setVisible(false);

            RestaurantFrame.getMenuLabel().setVisible(true);
            RestaurantFrame.getOrderLabel().setVisible(false);

            RestaurantFrame.getWelcomeLabel().setVisible(false);
            RestaurantFrame.getRoleLabel().setVisible(false);

            RestaurantFrame.getOrderScrollPane().setVisible(false);
            RestaurantFrame.getNoOrdersLabel().setVisible(false);

            RestaurantFrame.getChefScrollPane().setVisible(false);

            RestaurantFrame.displayMenuTable();
        }
    }

    private class WaiterButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            RestaurantFrame.getAdminButton().setBackground(Constants.Colors.ToolbarColor);
            RestaurantFrame.getWaiterButton().setBackground(Constants.Colors.ButtonsColor);
            RestaurantFrame.getChefButton().setBackground(Constants.Colors.ToolbarColor);
            RestaurantFrame.getHomePageButton().setBackground(Constants.Colors.ToolbarColor);

            RestaurantFrame.getAdminActionsPanel().setVisible(false);
            RestaurantFrame.getWaiterActionsPanel().setVisible(true);

            RestaurantFrame.getMenuLabel().setVisible(false);
            RestaurantFrame.getOrderLabel().setVisible(true);
            RestaurantFrame.getToCookLabel().setVisible(false);
            RestaurantFrame.getNoMenuItemsLabel().setVisible(false);

            RestaurantFrame.getWelcomeLabel().setVisible(false);
            RestaurantFrame.getRoleLabel().setVisible(false);

            RestaurantFrame.getMenuScrollPane().setVisible(false);
            RestaurantFrame.getChefScrollPane().setVisible(false);

            RestaurantFrame.displayOrderTable();
        }
    }

    private class ChefButtonActionLisneter implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            RestaurantFrame.getAdminButton().setBackground(Constants.Colors.ToolbarColor);
            RestaurantFrame.getWaiterButton().setBackground(Constants.Colors.ToolbarColor);
            RestaurantFrame.getChefButton().setBackground(Constants.Colors.ButtonsColor);
            RestaurantFrame.getHomePageButton().setBackground(Constants.Colors.ToolbarColor);

            RestaurantFrame.getAdminActionsPanel().setVisible(false);
            RestaurantFrame.getWaiterActionsPanel().setVisible(false);

            RestaurantFrame.getMenuLabel().setVisible(false);
            RestaurantFrame.getOrderLabel().setVisible(false);
            RestaurantFrame.getToCookLabel().setVisible(false);

            RestaurantFrame.getWelcomeLabel().setVisible(false);
            RestaurantFrame.getRoleLabel().setVisible(false);

            RestaurantFrame.getOrderScrollPane().setVisible(false);
            RestaurantFrame.getNoOrdersLabel().setVisible(false);

            RestaurantFrame.getMenuScrollPane().setVisible(false);

            RestaurantFrame.deleteChefTable();
            RestaurantFrame.displayChefTable();
        }
    }

    private class HomePageActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            RestaurantFrame.getAdminButton().setBackground(Constants.Colors.ToolbarColor);
            RestaurantFrame.getWaiterButton().setBackground(Constants.Colors.ToolbarColor);
            RestaurantFrame.getChefButton().setBackground(Constants.Colors.ToolbarColor);
            RestaurantFrame.getHomePageButton().setBackground(Constants.Colors.ButtonsColor);

            RestaurantFrame.getAdminActionsPanel().setVisible(false);
            RestaurantFrame.getWaiterActionsPanel().setVisible(false);

            RestaurantFrame.getMenuLabel().setVisible(false);
            RestaurantFrame.getOrderLabel().setVisible(false);
            RestaurantFrame.getToCookLabel().setVisible(false);

            RestaurantFrame.getNoMenuItemsLabel().setVisible(false);
            RestaurantFrame.getNoOrdersLabel().setVisible(false);

            RestaurantFrame.getWelcomeLabel().setVisible(true);
            RestaurantFrame.getRoleLabel().setVisible(true);

            RestaurantFrame.getMenuScrollPane().setVisible(false);
            RestaurantFrame.getOrderScrollPane().setVisible(false);
            RestaurantFrame.getChefScrollPane().setVisible(false);
        }
    }

    private class NewItemButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            new AddMenuItemController(new AddMenuItemDialog());
        }
    }

    private class EditExistingItemButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (RestaurantFrame.getMenuTable().getSelectedRow() != -1) {
                new EditExistingItemController(new EditDialog());
            } else {
                new MessageDialog("Please select a product!");
            }
        }
    }

    private class DeleteItemActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (RestaurantFrame.getMenuTable().getSelectedRow() != -1) {
                new DeleteDialog();
            } else {
                new MessageDialog("Please select a product!");
            }
        }
    }

    private class CreateOrderButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
           if (RestaurantFrame.getRestaurant().getMenuItems().isEmpty()) {
               new MessageDialog("ERROR : Menu is empty");
           } else {
               new NewOrderController(new NewOrderDialog());
           }
        }
    }

    private class ComputePriceButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (RestaurantFrame.getOrderTable().getSelectedRows().length == 1) {
                if (String.valueOf(RestaurantFrame.getOrderTable().getModel().getValueAt(RestaurantFrame.getOrderTable().getSelectedRow(), 3)).equals("-")) {
                    RestaurantFrame.deleteOrderTable();
                    int orderId = (int) RestaurantFrame.getOrderTable().getModel().getValueAt(RestaurantFrame.getOrderTable().getSelectedRow(), 0);
                    for (Order order : RestaurantFrame.getRestaurant().getOrders().keySet()) {
                        if (order.getId() == orderId) {
                            RestaurantFrame.getRestaurant().computePrice(order);
                            order.setPrice(RestaurantFrame.getRestaurant().computePrice(order));
                            break;
                        }
                    }
                    RestaurantFrame.displayOrderTable();
                } else {
                    new MessageDialog("Price already computed!");
                }
            } else if (RestaurantFrame.getOrderTable().getSelectedRows().length > 1) {
                new MessageDialog("Please select just an order!");
            } else {
                new MessageDialog("Please select an order!");
            }
        }
    }

    private class GenerateBillActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (RestaurantFrame.getOrderTable().getSelectedRow() != -1) {
                if (RestaurantFrame.getOrderTable().getModel().getValueAt(RestaurantFrame.getOrderTable().getSelectedRow(), 3).equals("-")) {
                    new MessageDialog("Plese compute price for this order!");
                } else {
                    int orderId = (int) RestaurantFrame.getOrderTable().getModel().getValueAt(RestaurantFrame.getOrderTable().getSelectedRow(), 0);
                    for (Order order : RestaurantFrame.getRestaurant().getOrders().keySet()) {
                        if (order.getId() == orderId) {
                            RestaurantFrame.getRestaurant().generateBill(order);
                            break;
                        }
                    }
                }
            } else {
                new MessageDialog("Please select an order!");
            }
        }
    }
}
