package DAL;

import BLL.MenuItem;
import BLL.Order;
import Presentation.Views.RestaurantFrame;

import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FileWritter {

    private Order order;
    private ArrayList<MenuItem> orderedItems;
    private Map<MenuItem, Integer> billItems;
    private String billName;

    public FileWritter(Order order, ArrayList<MenuItem> orderedItems) {
        this.order = order;
        this.orderedItems = orderedItems;
        this.billName = "Bill " + order.getId() + ".txt";
        this.billItems = completeMap(this.orderedItems);
    }

    public static Map<MenuItem, Integer> completeMap(ArrayList<MenuItem> orderedItems) {
        Map<MenuItem, Integer> toBillItems = new HashMap<>();
        boolean finded = false;

        for (MenuItem menuItem : orderedItems) {
            finded = false;

            if (toBillItems.keySet().isEmpty()) {
                toBillItems.put(menuItem, 1);
            } else {
                for (MenuItem menuItem1 : toBillItems.keySet()) {
                    if (menuItem.equals(menuItem1)) {
                        toBillItems.put(menuItem1, toBillItems.get(menuItem1) + 1);
                        finded = true;
                        break;
                    }
                }

                if (finded == false) {
                    toBillItems.put(menuItem, 1);
                }
            }
        }

        return toBillItems;
    }

    public void writeData() {
        File file = new File(this.billName);

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));

            bufferedWriter.write("Bill for order " + this.order.getId());
            bufferedWriter.newLine();

            bufferedWriter.write("Date: " + this.order.getDate());
            bufferedWriter.newLine();

            bufferedWriter.write("Table: " + this.order.getTable());
            bufferedWriter.newLine();

            bufferedWriter.newLine();

            bufferedWriter.write("Products: ");
            bufferedWriter.newLine();

            for (MenuItem menuItem : this.billItems.keySet()) {
                bufferedWriter.write(menuItem.getName() + " (" + menuItem.getPrice() + ") x" + this.billItems.get(menuItem) + " : " + menuItem.computePrice() * this.billItems.get(menuItem));
                bufferedWriter.newLine();
            }

            bufferedWriter.newLine();

            bufferedWriter.write("Total: " + RestaurantFrame.getOrderTable().getModel().getValueAt(RestaurantFrame.getOrderTable().getSelectedRow(), 3));
            bufferedWriter.newLine();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
