package DAL;

import BLL.MenuItem;
import Run.MainClass;

import java.io.*;
import java.util.HashSet;

public class RestaurantSerilialization {

    public HashSet<MenuItem> read(String fileName) {
        HashSet<MenuItem> menuItems = new HashSet<>();

        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            menuItems = (HashSet<MenuItem>) objectInputStream.readObject();

            objectInputStream.close();
            fileInputStream.close();
        } catch (FileNotFoundException fileNotFoundException) {
            System.out.println("File not found!");
            System.out.println("A file named: " + MainClass.fileName + " will be created!");
            new File(MainClass.fileName);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }

        return menuItems;
    }

    public void write(HashSet<MenuItem> menuItems, String fileName) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(fileName);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(menuItems);

            objectOutputStream.close();
            fileOutputStream.close();
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
