package BLL.Interfaces;

import BLL.MenuItem;
import BLL.Order;

import java.util.ArrayList;

public interface IRestaurantProcessing {

    /**
     * @param menuItem
     * @pre menuItem != null
     * @pre menuItems.contains(menuItem) == false
     * @post size() = size()@pre + 1
     */
    void createMenuItem(MenuItem menuItem);

    /**
     * @param menuItem
     * @pre menuItem != null
     * @pre menuItems.contains(menuItem) == true
     * @post size() == size()@pre + 1
     */
    void deleteMenuItem(MenuItem menuItem);

    /**
     * @param oldItem
     * @param newItem
     * @ore oldItems != null && newItem != null
     * @pre menuItems.contains(oldMenuItem) == true
     * @post size() == size()@pre
     */
    void editMenuItem(MenuItem oldItem, MenuItem newItem);

    /**
     * @param order
     * @param orderedItems
     * @pre order != null && orderedItems != null
     * @post size() = size()@pre + 1
     */
    void createNewOrder(Order order, ArrayList<MenuItem> orderedItems);

    /**
     * @param order - order for which the bill will be computed
     * @pre order != null
     * @pre orderdItems != null
     */
    float computePrice(Order order);

    /**
     * @param order - order for which the bill will be computed
     * @pre order != null
     * @pre orderedItems != null
     */
    void generateBill(Order order);

    /**
     * @pre menuItems.contains(null) == false
     * @return
     */
    boolean checkInvariant();
}
