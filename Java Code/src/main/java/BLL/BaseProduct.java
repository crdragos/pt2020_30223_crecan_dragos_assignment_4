package BLL;

import java.util.HashSet;

public class BaseProduct extends MenuItem {

    public BaseProduct(String name, Float price) {
        super(name, price);
    }

    @Override
    public Float computePrice() {
        return this.getPrice();
    }

    @Override
    public HashSet<MenuItem> getComponents() {
        return null;
    }

    @Override
    public void setComponents(HashSet<MenuItem> menuItems) {

    }
}
