package BLL;

import java.util.Date;

public class Order {

    static int orderId = 0;
    private int id;
    private Date date;
    private int table;
    private float price;

    public Order(int table) {
        orderId++;
        this.id = orderId;
        this.date = new Date(System.currentTimeMillis());
        this.table = table;
        this.price = 0.0f;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + this.date.hashCode();
        result = 31 * result + this.id;
        result = 31 * result + this.table;

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Order)) {
            return false;
        }

        Order order = (Order) obj;
        if (this.id == order.id && this.date.equals(order.date) && this.table == order.table) {
            return true;
        }

        return false;
    }

    public int getId() {
        return this.id;
    }

    public Date getDate() {
        return this.date;
    }

    public int getTable() {
        return this.table;
    }

    public float getPrice() {
        return this.price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
