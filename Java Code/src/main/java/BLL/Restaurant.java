package BLL;

import BLL.Interfaces.IRestaurantProcessing;
import DAL.FileWritter;
import DAL.RestaurantSerilialization;
import Presentation.Controllers.RestaurantController;
import Run.MainClass;

import java.util.*;

public class Restaurant extends Observable implements IRestaurantProcessing {

    private RestaurantSerilialization restaurantSerilialization;
    private Map<Order, ArrayList<MenuItem>> orders;
    private HashSet<MenuItem> menuItems;

    public Restaurant() {
        this.restaurantSerilialization = new RestaurantSerilialization();
        if (restaurantSerilialization.read(MainClass.fileName) == null) {
            this.menuItems = new HashSet<>();
        } else {
            this.menuItems = restaurantSerilialization.read(MainClass.fileName);
        }
        this.orders = new HashMap<>();
        this.addObserver(RestaurantController.restaurantFrame);
    }

    public HashSet<MenuItem> getMenuItems() {
        return menuItems;
    }

    public Map<Order, ArrayList<MenuItem>> getOrders() {
        return this.orders;
    }

    public RestaurantSerilialization getRestaurantSerilialization() {
        return restaurantSerilialization;
    }

    public void setMenuItems(HashSet<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public MenuItem search(String name) {
        for (MenuItem menuItem : this.menuItems) {
            if (menuItem.getName().equals(name)) {
                return menuItem;
            }
        }

        return null;
    }

    @Override
    public void createMenuItem(MenuItem menuItem) {
        assert menuItem != null;
        assert !menuItems.contains(menuItem);
        int size = menuItems.size();

        this.menuItems.add(menuItem);
        this.restaurantSerilialization.write(this.menuItems, MainClass.fileName);

        assert menuItems.size() == size + 1;
        assert checkInvariant();
    }

    @Override
    public void deleteMenuItem(MenuItem menuItem) {
        assert menuItem != null;
        assert menuItems.contains(menuItem);
        int size = menuItems.size();

        HashSet<MenuItem> productsWichContains = new HashSet<>();
        deleteAllAppearences(menuItem, productsWichContains);

        this.menuItems.removeAll(productsWichContains);
        this.menuItems.remove(menuItem);
        this.restaurantSerilialization.write(this.menuItems, MainClass.fileName);

        assert menuItems.size() == size - 1;
        assert checkInvariant();
    }

    private void deleteAllAppearences(MenuItem menuItem, HashSet<MenuItem> productsWichContains) {
        for (MenuItem menuItem1 : this.menuItems) {
            if (menuItem1 instanceof CompositeProduct && ((CompositeProduct) menuItem1).containsProduct(menuItem)) {
                productsWichContains.add(menuItem1);
                deleteAllAppearences(menuItem1, productsWichContains);
            }
        }
    }

    @Override
    public void editMenuItem(MenuItem oldItem, MenuItem newItem) {
        assert oldItem != null && newItem != null;
        assert menuItems.contains(oldItem);
        int size = menuItems.size();

        if (oldItem instanceof BaseProduct) {
            for (MenuItem menuItem : this.menuItems) {
                if (menuItem.equals(oldItem)) {
                    menuItem.setName(newItem.getName());
                    menuItem.setPrice(newItem.getPrice());
                    break;
                }
            }
        } else if (oldItem instanceof CompositeProduct) {
            for (MenuItem menuItem : this.menuItems) {
                if (menuItem.equals(oldItem)) {
                    menuItem.setName(newItem.getName());
                    menuItem.setComponents(newItem.getComponents());
                    break;
                }
            }
        }
        for (MenuItem menuItem : this.menuItems) {
            menuItem.setPrice(menuItem.computePrice());
        }
        this.restaurantSerilialization.write(this.menuItems, MainClass.fileName);

        assert menuItems.size() == size;
        assert checkInvariant();
    }

    @Override
    public void createNewOrder(Order order, ArrayList<MenuItem> orderedItems) {
        assert order != null && orderedItems != null;
        assert order.getTable() > 0;
        int size = orders.keySet().size();

        Order newOrder = order;
        this.orders.put(newOrder, orderedItems);

        setChanged();
        notifyObservers(orderedItems);

        assert orders.keySet().size() == size + 1;
        assert checkInvariant();
    }

    @Override
    public float computePrice(Order order) {
        assert order != null;

        float price = 0.0f;
        for (Order order1 : this.orders.keySet()) {
            if (order1.equals(order)) {
                for (MenuItem menuItem : this.orders.get(order1)) {
                    price += menuItem.computePrice();
                }
                break;
            }
        }
        return price;
    }

    @Override
    public void generateBill(Order order) {
        assert order != null;
        for (Order order1 : this.orders.keySet()) {
            if (order1.equals(order)) {
                FileWritter fileWritter = new FileWritter(order1, this.orders.get(order1));
                fileWritter.writeData();
                break;
            }
        }
    }

    @Override
    public boolean checkInvariant() {
        assert !menuItems.contains(null);

        return !menuItems.contains(null);
    }
}
