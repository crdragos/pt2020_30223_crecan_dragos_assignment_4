package BLL;

import java.util.HashSet;

public class CompositeProduct extends MenuItem {

    private HashSet<MenuItem> components;

    public CompositeProduct(String name, HashSet<MenuItem> components) {
        super(name, 0.0f);
        this.components = components;
        this.setPrice(this.computePrice());
    }

    public boolean containsProduct(MenuItem menuItem) {
        if (this.components.contains(menuItem)) {
            return true;
        }
        return false;
    }


    @Override
    public HashSet<MenuItem> getComponents() {
        return  this.components;
    }

    @Override
    public void setComponents(HashSet<MenuItem> menuItems) {
        this.components = menuItems;
    }

    @Override
    public Float computePrice() {
        Float totalPrice = 0.0f;
        for (MenuItem menuItem : this.components) {
            totalPrice += menuItem.getPrice();
        }

        return totalPrice;
    }
}
