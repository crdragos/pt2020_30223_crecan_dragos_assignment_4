package BLL;

import java.io.Serializable;
import java.util.HashSet;

public abstract class MenuItem implements Serializable {

    private String name;
    private Float price;

    public MenuItem(String name, Float price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public abstract Float computePrice();

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + Float.floatToIntBits(price);
        return result;
    }

    public abstract HashSet<MenuItem> getComponents();
    public abstract void setComponents(HashSet<MenuItem> menuItems);
}
