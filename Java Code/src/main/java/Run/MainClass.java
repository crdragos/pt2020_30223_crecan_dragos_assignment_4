package Run;

import Presentation.Controllers.RestaurantController;
import Presentation.Views.RestaurantFrame;

public class MainClass {
    public static String fileName;
    public static void main(String[] args) {
        fileName = args[0];
        new RestaurantController(new RestaurantFrame("TP Restaurant"));
    }
}
